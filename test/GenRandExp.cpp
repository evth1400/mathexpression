//
// Created by Eva on 2017-10-19.
// A Function to generate a random math expression to be used in the test scenario
//
//
//

#include <vector>
#include <string>
#include <sstream>
#include <random>
#include <stack>

using namespace std;

#ifndef RandomExpression_H
#define RandomExpression_H

class RandomExpression
{
private:
    vector<string> mathExpression;
    vector<string> postfix;

public:
    RandomExpression(default_random_engine &aGenerator);
    string getStringExpression();
    string getPostFix();
    float calculate();

};

inline RandomExpression::RandomExpression(default_random_engine &aGenerator)
{
    //Randomize how many operands
    uniform_int_distribution<int> numOpDist(2, 8);
    int numOperands = numOpDist(aGenerator);

    //randomize how many bracketts
    int topPossilbe = numOperands / 3;		//restricts how many there can be(0, 1 or 2)
    uniform_int_distribution<int> topPosDist(0, topPossilbe);
    int numBracketts = topPosDist(aGenerator);

    vector<int> openBrackPos, closeBrackPos;

    //generate brackettpositions(can wrap least of two operands)
    int startOp, endOp;
    uniform_int_distribution<int> posDist;

    switch (numBracketts)
    {
        case 1:
            posDist = uniform_int_distribution<int>(1, (numOperands - 1));
            startOp = posDist(aGenerator);

            posDist = uniform_int_distribution<int>((startOp + 1), numOperands);
            endOp = posDist(aGenerator);

            openBrackPos.push_back(startOp);
            closeBrackPos.push_back(endOp);
            break;
        case 2:
            //generate first
            posDist = uniform_int_distribution<int>(1, (numOperands - 3));
            startOp = posDist(aGenerator);

            posDist = uniform_int_distribution<int>((startOp + 1), (numOperands - 2));
            endOp = posDist(aGenerator);

            openBrackPos.push_back(startOp);
            closeBrackPos.push_back(endOp);

            //generate second
            posDist = uniform_int_distribution<int>((endOp + 1), (numOperands - 1));
            startOp = posDist(aGenerator);

            posDist = uniform_int_distribution<int>((startOp + 1), numOperands);
            endOp = posDist(aGenerator);

            openBrackPos.push_back(startOp);
            closeBrackPos.push_back(endOp);
            break;
        default:
            break;
    }

    //randomize operands and operators
    for (int opNum = 1; opNum <= numOperands; opNum++)
    {
        //is operand start bracket marked?
        for (auto brackettIdx : openBrackPos)
        {
            if (opNum == brackettIdx)
            {
                //operand is marked to be first inside bracket
                mathExpression.push_back("(");
            }
        }

        //create random operand
        uniform_int_distribution<int> operandDist(1, 100);	//top value of 100 * top times of 8 shouldnt be able to cause overflow
        int operand = operandDist(aGenerator);
        mathExpression.push_back(to_string(operand));

        //is operand end bracket marked?
        for (auto brackettIdx : closeBrackPos)
        {
            if (opNum == brackettIdx)
            {
                //operand is marked to be last inside bracket
                mathExpression.push_back(")");
            }
        }

        //add an operator
        if (opNum != numOperands)
        {
            //add operator
            uniform_int_distribution<int> operatorDist(1, 4);
            int op = operatorDist(aGenerator);
            switch (op)
            {
                case 1:
                    mathExpression.push_back("+");
                    break;
                case 2:
                    mathExpression.push_back("-");
                    break;
                case 3:
                    mathExpression.push_back("/");
                    break;
                case 4:
                    mathExpression.push_back("*");
                    break;
                default:
                    break;
            }
        }
    }

//==========Create postfix vector
    stack<string> mathStack;

    //read left to right
    for (size_t expPos = 0; expPos < mathExpression.size(); expPos++)
    {
        string posCont = mathExpression.at(expPos);

        //----is it a operator?
        if (posCont == "*" || posCont == "/")
        {
            //pop any '*' or '/' off the stack top.
            while (!mathStack.empty() && mathStack.top() == "*" || !mathStack.empty() && mathStack.top() == "/")
            {
                postfix.push_back(mathStack.top());
                mathStack.pop();
            }

            mathStack.push(posCont);			//push on stack
            continue;							//continue to next position
        }
        if (posCont == "+" || posCont == "-")
        {
            //always pop and push
            while (!mathStack.empty() && mathStack.top() != "(")
            {
                postfix.push_back(mathStack.top());
                mathStack.pop();
            }
            mathStack.push(posCont);
            continue;
        }

        //----is it a brackett?
        if (posCont == "(")
        {
            mathStack.push("(");
            continue;
        }
        if (posCont == ")")
        {
            while (mathStack.top() != "(")
            {
                postfix.push_back(mathStack.top());
                mathStack.pop();
            }
            mathStack.pop();			//pop the opening brackett
            continue;
        }

        //----then it is a number!
        postfix.push_back(posCont);
    }

    //pop what is left on stack
    while (!mathStack.empty())
    {
        postfix.push_back(mathStack.top());
        mathStack.pop();
    }
}

inline string RandomExpression::getStringExpression()
{
    ostringstream oss;

    for (auto idx : mathExpression)
    {
        oss << idx << " ";
    }

    return oss.str();
}

inline string RandomExpression::getPostFix()
{
    ostringstream oss;

    for (auto idx : postfix)
    {
        oss << idx << " ";
    }

    return oss.str();
}

inline float RandomExpression::calculate()
{
    //operand stack
    stack<float> operandStack;

    for (auto expPart : postfix)
    {
        if (expPart != "+" && expPart != "-" && expPart != "*" && expPart != "/")
        {
            operandStack.push(stof(expPart));
        }
        else
        {
            float right = operandStack.top();
            operandStack.pop();
            float left = operandStack.top();
            operandStack.pop();

            float tempRes = 0;

            //calc
            if (expPart == "+")
            {
                tempRes = left + right;
            }
            else if (expPart == "-")
            {
                tempRes = left - right;
            }
            else if (expPart == "*")
            {
                tempRes = left * right;
            }
            else if (expPart == "/")
            {
                tempRes = left / right;
            }

            //push back on stack
            operandStack.push(tempRes);
        }
    }

    return operandStack.top();
}

#endif