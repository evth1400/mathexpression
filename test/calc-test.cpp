//
// Created by Eva on 2017-10-19.
// För att veta om programmet producerar korrekta resultat används antagandet att ett givet värde ska producera ett specifikt resulat
//
#include "../include/catch.hpp"
#include "../include/MathExpression.h"
#include "GenRandExp.cpp"

//initialize random seed
default_random_engine generator(static_cast<unsigned>(time(0)));

SCENARIO("Creating a new valid MathExpression"){
    GIVEN("a valid expression value"){
        RandomExpression expGenerator(generator);
        string givenExp = expGenerator.getStringExpression();
        MathExpression mathExp(givenExp);

        THEN("you should be able to retrieve that expression value"){
            REQUIRE(mathExp.infixNotation() == givenExp);
        }

        THEN("no error message should be generated"){
            REQUIRE(mathExp.errorMessage() == "");
        }

        THEN("the isValid function should return true"){
            REQUIRE(mathExp.isValid() == true);
        }

        THEN("postFix should return value same as generator"){  //using own tested function to compare
            REQUIRE(mathExp.postfixNotation() == expGenerator.getPostFix());
        }

        THEN("Expression should calculate a correct result"){   //using own tested function to compare
            Approx target = Approx(expGenerator.calculate()).epsilon(0.01);     //1% miscalculation margin
            REQUIRE(mathExp.calculate() == target);
        }

        WHEN("A new valid expression has been assigned"){
            RandomExpression newExp(generator);
            givenExp = newExp.getStringExpression();
            mathExp = givenExp;

            THEN("Expression object should return new expression"){
                REQUIRE(mathExp.infixNotation()==givenExp);
            }

            THEN("There should still be no error message"){
                REQUIRE(mathExp.errorMessage()=="");
            }
        }
    }
}

SCENARIO("Trying to create an invalid expression"){
    RandomExpression expGenerator(generator);
    string givenExp = expGenerator.getStringExpression();

    GIVEN("a random known illegal construct"){
        int illegalConst = rand() % 20;
        string whatIllegal = "";

        //inject the random "sabotage"
        uniform_int_distribution<int> posDist(0, givenExp.length()-1);

        switch(illegalConst){
            case 0:     //trailing operator
                uniform_int_distribution<int> opSelect(0, 3);
                int op = opSelect(generator);    //allows for tests with different operators
                switch (op){
                    case 0:
                        givenExp = givenExp.append("+");
                        break;
                    case 1:
                        givenExp = givenExp.append("-");
                        break;
                    case 2:
                        givenExp = givenExp.append("*");
                        break;
                    case 3:
                        givenExp = givenExp.append("/");
                        break;
                }
                whatIllegal = " an illegal trailing operator";
                break;
            case 1:     //Containing illegal characters
                char illg[] = {'a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s',
                               't','u','v','w','x','y','z','A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P',
                                'Q','V','W','X','Y','X','!','%','^'};   //just a selection of some(not all) illegal chars
                uniform_int_distribution<int> charSelect(0, sizeof(illg)-1);
                char bandit = illg [charSelect(generator)];
                //inject a letter somewhere
                givenExp.insert(posDist(generator), 1 ,bandit);
                whatIllegal=" an illegal character";
                break;
            case 2:     //Adjoining brackets
                string banditBrack = ")(";
                givenExp.insert(posDist(generator),banditBrack);
                whatIllegal = " adjoining brackets";
                break;
            case 3:     //uneven number of brackets
                char b[]{'(',')'};
                uniform_int_distribution<int> bType(0, 1);
                char Brack = b[bType(generator)];
                //insert
                givenExp.insert(posDist(generator), 1 ,bandit);
                whatIllegal = " a stray bracket";
                break;
            case 4:     //Missing operator
                for(int chIdx = 0; chIdx < givenExp.length(); chIdx++){
                    char ch = givenExp[chIdx];
                    if(ch == '+' || ch == '-' || ch == '*' || ch == '/'){
                        givenExp.erase(chIdx, 1);   //remove
                        givenExp.insert(chIdx, 1, ' '); //insert space
                        whatIllegal = " a missing operator";
                        break;
                    }
                }
                break;
            case 5:     //duplicate operators
                for(int chIdx = 0; chIdx < givenExp.length(); chIdx++){
                    char ch = givenExp[chIdx];
                    if(ch == '+' || ch == '-' || ch == '*' || ch == '/'){
                        givenExp.insert(posDist(generator), 1 ,'+');
                        whatIllegal = " duplicate operators";
                        break;
                    }
                }
                break;
        }
        MathExpression mathExp(givenExp);

        THEN(string("An expression containing ").append(whatIllegal).append(" should fail to validate.")){
            REQUIRE(mathExp.isValid() == false);
        }

        THEN("The error message should contain a string"){
            REQUIRE(mathExp.errorMessage().length()>0);
        }

        THEN("PostFix should throw an exception"){
            REQUIRE_THROWS(mathExp.postfixNotation());
        }

        THEN("Calculating should throw an exception"){
            REQUIRE_THROWS(mathExp.calculate());
        }

        WHEN("The invalid expression object is assigned a new valid expression"){
            RandomExpression newExpression(generator);
            mathExp = newExpression.getStringExpression();

            THEN("The object should return the new expression"){
                REQUIRE(mathExp.infixNotation() == newExpression.getStringExpression());
            }

            THEN("The error message should have been removed"){
                REQUIRE(mathExp.errorMessage()=="");
            }

            THEN("Expression should validate"){
                REQUIRE(mathExp.isValid());
            }

            THEN("PostFix shoudn't throw an exception"){
                REQUIRE_NOTHROW(mathExp.postfixNotation());
            }

            THEN("calculating shoudn't throw an exception"){
                REQUIRE_NOTHROW(mathExp.calculate());
            }
        }
    }
}